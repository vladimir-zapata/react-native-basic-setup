import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import {Text, View} from 'react-native';
import {RootStackParamList} from '../App';

type Props = NativeStackScreenProps<RootStackParamList, 'Home'>;

const HomeScreen = ({}: Props) => {
  return (
    <View>
        <Text>Home Screen</Text>
    </View>
  );
};

// const styles = StyleSheet.create({});

export default HomeScreen;
